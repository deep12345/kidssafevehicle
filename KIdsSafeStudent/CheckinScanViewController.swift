//
//  CheckinScanViewController.swift
//  KIdsSafeStudent
//
//  Created by Mac on 01/07/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import AVFoundation
import IBAnimatable

class CheckinScanViewController: UIViewController {

    @IBOutlet weak var cameraview: UIView!
       
     
    @IBOutlet weak var blurimgview: UIView!
    
    @IBOutlet weak var viewStudentNotFounf: AnimatableView!
    
    
    @IBOutlet weak var viewCheckinSuccessful: AnimatableView!
    
       var captureSession: AVCaptureSession!
       var stillImageOutput: AVCapturePhotoOutput!
       var videoPreviewLayer: AVCaptureVideoPreviewLayer!
       
      var Ischeckin = Bool()
    
       
       override func viewDidLoad() {
           super.viewDidLoad()
           
           
           
    
       }
      
        override func viewDidAppear(_ animated: Bool) {
            super.viewDidAppear(animated)
             // Setup your camera here...
           
           captureSession = AVCaptureSession()
           captureSession.sessionPreset = .medium
           
           guard let backCamera = AVCaptureDevice.default(for: AVMediaType.video)
               else {
                   print("Unable to access back camera!")
                   return
           }
          do {
              let input = try AVCaptureDeviceInput(device: backCamera)
              //Step 9
            stillImageOutput = AVCapturePhotoOutput()
                   
                   if captureSession.canAddInput(input) && captureSession.canAddOutput(stillImageOutput) {
                       captureSession.addInput(input)
                       captureSession.addOutput(stillImageOutput)
                       setupLivePreview()
                   }
           
          }
          catch let error  {
              print("Error Unable to initialize back camera:  \(error.localizedDescription)")
          }
           
           
          
           
           
        }
       
    
    
    
       func setupLivePreview() {
           
           videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
           
           videoPreviewLayer.videoGravity = .resizeAspect
           videoPreviewLayer.connection?.videoOrientation = .portrait
           cameraview.layer.addSublayer(videoPreviewLayer)
          
           //Step12
           DispatchQueue.global(qos: .userInitiated).async { //[weak self] in
                      self.captureSession.startRunning()
              //Step 13
               DispatchQueue.main.async {
                              self.videoPreviewLayer.frame = self.cameraview.bounds
                          }
                  }
           
           
       }
       override func viewWillDisappear(_ animated: Bool) {
           super.viewWillDisappear(animated)
           self.captureSession.stopRunning()
       }
       
       @IBAction func btn_captureimage(_ sender: Any) {
        if Ischeckin {
               viewCheckinSuccessful.isHidden = false
                   blurimgview.isHidden = false
               }
               else{
                   viewStudentNotFounf.isHidden = false
                   blurimgview.isHidden = false
               }
        
       }
       
       
       @IBAction func btn_back(_ sender: Any) {
           navigationController?.popViewController(animated: true)
       }
       
       /*
       // MARK: - Navigation

       // In a storyboard-based application, you will often want to do a little preparation before navigation
       override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
           // Get the new view controller using segue.destination.
           // Pass the selected object to the new view controller.
       }
       */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    
    
    
    
    
    
    @IBAction func btn_close(_ sender: Any) {
        viewCheckinSuccessful.isHidden = true
        blurimgview.isHidden = true
        viewStudentNotFounf.isHidden = true
    }
    
    @IBAction func btn_LetsGo(_ sender: Any) {
        
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
               
    }
    
    @IBAction func btnFindStudent(_ sender: Any) {
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "StudentListViewController") as! StudentListViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    
}
