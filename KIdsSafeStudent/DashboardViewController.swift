//
//  DashboardViewController.swift
//  KIdsSafeStudent
//
//  Created by Mac on 27/06/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import IBAnimatable

class DashboardCell : UITableViewCell {
    
    @IBOutlet var lblname: UILabel!
    @IBOutlet var lblstudentid: UILabel!
    
    @IBOutlet weak var lblimage: UIImageView!
    
    @IBOutlet weak var vwMiddle: AnimatableView!
    
    override func awakeFromNib() {
         
      }
    
}



class DashboardViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UISearchBarDelegate
{
    
    
    @IBOutlet weak var tblRecentActivityList: UITableView!
    var  arrRecentActivitylist = [[String:Any]]()
    
    
    @IBOutlet weak var viewBlur: UIView!
    
    @IBOutlet weak var txtsearch: UISearchBar!
    @IBOutlet weak var otpView: AnimatableView!
    
    @IBOutlet weak var viewCheckinSuccessfull: AnimatableView!
    
    @IBOutlet weak var viewSearchStudent: AnimatableView!
    
    @IBOutlet weak var viewInScanTiming: AnimatableView!
    
    @IBOutlet weak var ViewOutScanTiming: AnimatableView!
    
    @IBOutlet weak var ViewMyProfike: AnimatableView!
    
    
    
    var tapGesture = UITapGestureRecognizer()
    var tapOutScanGesture = UITapGestureRecognizer()
    var tapMyProfileScanGesture = UITapGestureRecognizer()
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrRecentActivitylist.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:"DashboardCell", for: indexPath) as! DashboardCell
        
        // ("\(arrWishlist[indexPath.row]["ProductName"] as? String ?? "")")
        
        return cell
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()

        tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.myviewTapped(_:)))
              tapGesture.numberOfTapsRequired = 1
              tapGesture.numberOfTouchesRequired = 1
              viewInScanTiming.addGestureRecognizer(tapGesture)
              viewInScanTiming.isUserInteractionEnabled = true
        
        tapOutScanGesture = UITapGestureRecognizer(target: self, action: #selector(self.myOutScanTapped(_:)))
        tapOutScanGesture.numberOfTapsRequired = 1
        tapOutScanGesture.numberOfTouchesRequired = 1
        ViewOutScanTiming.addGestureRecognizer(tapOutScanGesture)
        ViewOutScanTiming.isUserInteractionEnabled = true
        
          tapMyProfileScanGesture = UITapGestureRecognizer(target: self, action: #selector(self.myProfileviewTapped(_:)))
                 tapOutScanGesture.numberOfTapsRequired = 1
                 tapMyProfileScanGesture.numberOfTouchesRequired = 1
            ViewMyProfike.addGestureRecognizer(tapMyProfileScanGesture)
                 ViewMyProfike.isUserInteractionEnabled = true
        
        txtsearch.delegate = self as? UISearchBarDelegate
        // Do any additional setup after loading the view.
    }
    
    @objc func myOutScanTapped(_ sender: UITapGestureRecognizer) {
   
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "CheckinScanViewController") as! CheckinScanViewController
        nextViewController.Ischeckin = false
     self.navigationController?.pushViewController(nextViewController, animated: true)
       
    }
   @objc func myProfileviewTapped(_ sender: UITapGestureRecognizer) {
    
    let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "MyProfileViewController") as! MyProfileViewController
    self.navigationController?.pushViewController(nextViewController, animated: true)
       
    }
   @objc func myviewTapped(_ sender: UITapGestureRecognizer) {
   
    let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "CheckinScanViewController") as! CheckinScanViewController
    nextViewController.Ischeckin = true
    self.navigationController?.pushViewController(nextViewController, animated: true)
    
    }
  
    
    func searchBar(_ searchBar: UISearchBar,
    textDidChange searchText: String)
    {
   
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "StudentListViewController") as!StudentListViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    
    @IBAction func btn_inscantime(_ sender: Any) {
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "CheckinScanViewController") as! CheckinScanViewController
           nextViewController.Ischeckin = true
           self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    
    @IBAction func btn_outscantime(_ sender: Any) {
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "CheckinScanViewController") as! CheckinScanViewController
               nextViewController.Ischeckin = false
            self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    
    
    @IBAction func btn_myProfile(_ sender: Any) {
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "MyProfileViewController") as! MyProfileViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    @IBAction func btn_myprofile(_ sender: Any) {
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "MyProfileViewController") as! MyProfileViewController
        self.navigationController?.pushViewController(nextViewController, animated: true)
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
  
    func getRecentActivityList()
    {
        if !isInternetAvailable(){
        noInternetConnectionAlert(uiview: self)
                    }
            else
            {
                
                let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                    "X-BUYSELL-LOGIN-TOKEN":UserDefaults.standard.object(forKey: "login_token") ?? "" ,
                                      ] as [String : Any]
                                      
                let url = ServiceList.SERVICE_URL+ServiceList.RecentActivityList
                      
                                         callApi(url,method: .get,
                                                       extraHeader: header ,withLoader: true)
                                               { (result) in
                                                  //  print("LOGINRESPONSE:",result)
                                                    if result.getBool(key: "status")
                                                    {
                                             
                let RecentActivitylist = result["data"] as? [String:Any] ?? [:]
                self.arrRecentActivitylist = RecentActivitylist["wishlist"] as? [[String:Any]] ?? []
                print(self.arrRecentActivitylist)
                self.tblRecentActivityList.reloadData()
                                               
                                                    }
                                                 else
                                                 {
                                                     showToast(uiview: self, msg: result.getString(key: "message"))
                                                 }
                                        }
                                    }
                                    
                                }
    
    
    @IBAction func btn_notifications(_ sender: Any) {
        
    }
    
    
    @IBAction func btn_closeCheckin(_ sender: Any) {
        
    }
    
    @IBAction func btn_closeOtp(_ sender: Any) {
   
    }
    
    
    @IBAction func btn_closeSearchStudent(_ sender: Any) {
        
    }
    
    
    @IBAction func btn_findStudent(_ sender: Any) {
    
    }
    
    
    @IBAction func btn_checkin(_ sender: Any) {
        
    }
}
