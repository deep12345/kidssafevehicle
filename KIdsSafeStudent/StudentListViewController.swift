//
//  StudentListViewController.swift
//  KIdsSafeStudent
//

//  Created by Mac on 27/06/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import IBAnimatable
import SVPinView

class StudentList: UITableViewCell {
    
    @IBOutlet var lblname: UILabel!
    @IBOutlet var lblstudentid: UILabel!
    
    @IBOutlet weak var lblimage: UIImageView!
    
    @IBOutlet weak var vwMiddle: AnimatableView!
    
    override func awakeFromNib() {
         
      }
    
}


class StudentListViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    
    
    @IBOutlet weak var viewOTp: AnimatableView!
    @IBOutlet weak var viewCheckinSuccess: AnimatableView!
    
    @IBOutlet weak var ViewBlur: UIView!
    var emailid = ""
    var OTP = ""
    var  arrStudentslist = [[String:Any]]()
    
    @IBOutlet weak var tblstudentList: UITableView!
    
    
    @IBOutlet weak var pinView: SVPinView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
 
        
        //  getStudentList()
        
        
      
        // Do any additional setup after loading the view.
    }
    

    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier:"StudentList", for: indexPath) as! StudentList
//        cell.lblname.text = (" \(arrStudentslist[indexPath.row]["first_name"] as? String ?? "")")  + (" \(arrStudentslist[indexPath.row]["last_name"] as? String ?? "")")
//        let url =  ("\(arrStudentslist[indexPath.row]["DefaultImageURL"] as? String ?? "")")
//        cell.lblimage.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: "Rectangle 43.png"))
        return cell
    
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

       
       // emailid = ("\(arrStudentslist[indexPath.row]["email"] as? String ?? "")")
        emailid = "ramizg.aipl@gmail.com"
        SendOtp()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func btn_back(_ sender: Any) {
         navigationController?.popViewController(animated: true)
        
    }
    
    
    @IBAction func btn_verifyEmail(_ sender: Any) {
    
     
       OTP  =    pinView.getPin()
       
       VerifyOtp()
    
    }

    
    
    
    @IBAction func btn_close(_ sender: Any) {
        
        ViewBlur.isHidden = true
       viewOTp.isHidden = true
        
    }
    
    
    
    
    func getStudentList()
       {
           if !isInternetAvailable(){
           noInternetConnectionAlert(uiview: self)
                       }
               else
               {
                    let parameters = ["email" : UserDefaults.standard.getUserDict()["email"] as? String ?? "", "password" :UserDefaults.standard.getUserDict()["password"] as? String ?? "","device_token" : UIDevice.current.identifierForVendor!.uuidString]
               
                let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                "X-KIDSAFE-LOGIN-TOKEN":UserDefaults.standard.object(forKey: "login_token") ?? ""
                                         ] as [String : Any]
                                         
                   let url = ServiceList.SERVICE_URL+ServiceList.StudentList_API
                         
                    callApi(url,method: .get,
                            param: parameters, extraHeader: header,
                            withLoader: true)
                                                  { (result) in
                                                     //  print("LOGINRESPONSE:",result)
                                                       if result.getBool(key: "status")
                                                       {
                                                
          
        self.arrStudentslist = result["data"] as? [[String:Any]] ?? []
        print(self.arrStudentslist)
        self.tblstudentList.reloadData()
                      
                                                       
                                                       }
                                        else
                                                    {
                                                        showToast(uiview: self, msg: result.getString(key: "message"))
                                                    }
                                           }
                                       }
                                       
                                   }
    
    
    
    
      func SendOtp()
                {
                    if !isInternetAvailable(){
                        noInternetConnectionAlert(uiview: self)
                    }
                    else
                    {
                     let parameters = ["email" :emailid
                            ] as [String : Any]
                      let url = ServiceList.SERVICE_URL+ServiceList.SendOtp
                        let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                        "X-KIDSAFE-LOGIN-TOKEN":UserDefaults.standard.object(forKey: "login_token") ?? ""
                                                               ] as [String : Any]
                        
                         callApi(url,method: .post,param: parameters,extraHeader: header, withLoader: true)
                               { (result) in
                                    print("LOGINRESPONSE:",result)
                                    if result.getBool(key: "status")
                                    {
                                  
                                     
                                   //  dictuser["login_token"] = dict.getString(key: "login_token")
                                     self.ViewBlur.isHidden = false
                                     self.viewOTp.isHidden = false
                                     showToast(uiview: self, msg: result.getString(key: "message"))
                                        
                                        
                                    
                                    }
                                 else
                                    {
                                     showToast(uiview: self, msg: result.getString(key: "message"))
                                 }
                        }
                    }
                    
                }
    
    func VerifyOtp()
    {
        if !isInternetAvailable(){
            noInternetConnectionAlert(uiview: self)
        }
        else
        {
         let parameters = ["email" :emailid,
                           "otp" : OTP
                ] as [String : Any]
            
            let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "","X-KIDSAFE-LOGIN-TOKEN":UserDefaults.standard.object(forKey: "login_token") ?? ""  ] as [String : Any]
          let url = ServiceList.SERVICE_URL+ServiceList.VerifyOtp
             callApi(url,method: .post,param: parameters,extraHeader: header, withLoader: true)
                   { (result) in
                        print("LOGINRESPONSE:",result)
                        if result.getBool(key: "status")
                        {
                      
                         
                       //  dictuser["login_token"] = dict.getString(key: "login_token")
                        self.ViewBlur.isHidden = false
                        self.viewCheckinSuccess.isHidden = false
                        showToast(uiview: self, msg: result.getString(key: "message"))
                            
                            
                        
                        }
                     else
                        {
                         showToast(uiview: self, msg: result.getString(key: "message"))
                     }
            }
        }
        
    }
    
    @IBAction func btn_letsgo(_ sender: Any) {
        
    }
    
    
    @IBAction func btn_closecheckin(_ sender: Any) {
       
        self.ViewBlur.isHidden = true
        self.viewCheckinSuccess.isHidden = true
        
    }
    
    
    
    
}
