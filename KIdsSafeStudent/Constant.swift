//
//  Constant.swift
//  JetPayApp
//
//  Created by My Mac on 2/14/18.
//  Copyright © 2018 My Mac. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

struct ReversedGeoLocation {
    let name: String            // eg. Apple Inc.
    let streetName: String      // eg. Infinite Loop
   // let streetNumber: String    // eg. 1
    let city: String            // eg. Cupertino
    let state: String           // eg. CA
    let zipCode: String         // eg. 95014
    let country: String         // eg. United States
    let isoCountryCode: String  // eg. US

    var formattedAddress: String {
        return """
        \(name),
        \(streetName),
        \(city), \(state) \(zipCode)
        \(country)
        """
    }

    // Handle optionals as needed
    init(with placemark: CLPlacemark) {
        self.name           = placemark.name ?? ""
        self.streetName     = placemark.thoroughfare ?? ""
       // self.streetNumber   = placemark.subThoroughfare ?? ""
        self.city           = placemark.locality ?? ""
        self.state          = placemark.administrativeArea ?? ""
        self.zipCode        = placemark.postalCode ?? ""
        self.country        = placemark.country ?? ""
        self.isoCountryCode = placemark.isoCountryCode ?? ""
    }
}

struct MyClassConstants
{
    static var dictGlobalData: [String : AnyObject] = [:]
}

struct CustomerConstants
{
    static var user_id = String()
}

//MARK:- Screen Size

struct ScreenSize
{
    static let SCREEN_WIDTH = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT = UIScreen.main.bounds.size.height
    static let SCREEN_MAX_LENGTH = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}

struct DeviceType
{
    static let IS_IPHONE_4_OR_LESS =  UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
    static let IS_IPHONE_5 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let IS_IPHONE_6 = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let IS_IPHONE_6P = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    static let IS_IPHONE_X = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 812.0
}

//MARK:- Custom fonts
struct FontNames {
    
//    static let RajdhaniName = "rajdhani"
//    struct Rajdhani {
        static let RajdhaniBold = "rajdhani-bold"
        static let RajdhaniMedium = "rajdhani-medium"
//    }
}
struct GlobalColor {
    static var SelectedColor = UserDefaults.standard.color(forKey: "ColorCode")
}
struct FontColor {
    
    static let dayfont = 0x000000
    static let nightfont = 0xffffff
    
   
}

//MARK:- Webservices

 struct ServiceList
 {
   // static let SERVICE_URL = "https://buysell.je/api/v1/"
   // static var SERVICE_URL = "http://192.168.0.109/ci/buysell.je/api/v1/"
    
//    static var SERVICE_URL = UserDefaults.standard.string(forKey: "ApiURL")
//    static var SocketUrl = UserDefaults.standard.string(forKey: "ChatURL")
//    static var DomainUrl = UserDefaults.standard.string(forKey: "DomainName")
    static var SERVICE_URL = "http://clientsdemoarea.com/projects/kid_safe/api/v1/driver/"
    
    static let TERMS_CONDITION_API = "terms-conditions?mobile=1"
    
    static let SERVICE_AUTH = "auth/"

    static let X_SIMPLE_API_KEY = "kkcoswggwgwkkc8w4ok808o48kswc40c0www4wss"
    static let AUTHORIZATION_AUTH = "YWRtaW46QVBJQEtJRFNBRkVAMTIz"

    static let LOGIN_API = SERVICE_AUTH + "login"
    static let RecentActivityList = "requirment/wishlist"
    static let getProfile = SERVICE_AUTH + "driver_profile"
    static let UpdateProfile_API = SERVICE_AUTH + "update_profile"
    static let StudenList_API = "student/list"
    static let SendOtp = SERVICE_AUTH + "send_otp"
    static let VerifyOtp = SERVICE_AUTH + "verify_otp"
    
    
    
    
    
    static let FORGOTPASSWORD_API = SERVICE_AUTH + "forgot_password"
    static let CHANGEPASSWORD_API = SERVICE_AUTH + "change_password"
    static let SEND_OTP_API = SERVICE_AUTH + "send_otp"
    static let VERIFY_OTP_API = SERVICE_AUTH + "verify_otp"
    static let APP_REGISTER_API = SERVICE_AUTH + "app_register"
    static let PROFILE_API = SERVICE_AUTH + "profile"
    static let SOCIAL_LOGIN_API = SERVICE_AUTH + "social_login"
    static let LOGOUT_API = SERVICE_AUTH + "logout"
    static let DeActivate = SERVICE_AUTH + "deactive"
    static let ReActivate = SERVICE_AUTH + "reactive_account"

    static let PROFILE_UPDATE_API = "profile_update/profile"
    static let PRODUCT_EDIT_STEP_API = "products/edit_step"
    static let MY_PRODUCT_API = "products/my_product"
    static let CATEGORY_API = "categories/category"
    static let REQUIREMENT_SEARCHING_API = "requirment/serching"
    static let PRODUCT_VIEW_STEP_API = "products/view_step"
    static let PRODUCT_EDIT_PRODUCT_DATA_API = "products/edit_product_data"
    static let PRODUCT_EDIT_PRODUCT_API = "products/edit_product"
    static let REQUIREMENT_ADD_FAVOURITE_API = "requirment/add_faourtite"
    static let PEOPLE_LOOKINGFOR_LIST_API = "peoplelookingfor/peoplelookingfor_list"
    static let APP_SETTING_API = "appsettings/setting"
    static let LOCATION_CITY_API = "location/city"
    static let PRODUCT_API = "products/product"
    static let REQUIREMENT_LIST_API = "requirment/RequirmentList"
    static let REQUIREMENT_WISHLIST_API = "requirment/wishlist"
    static let REQUIREMENT_TRANSACTION_API = "requirment/transaction"
    
    static let PRODUCT_STEP_API = "products/step"
    static let REQUIREMENT_REPORTADD_API = "requirment/reportadd"
    static let REQUIREMENT_REMOVE_API = "requirment/remove"
    static let PRODUCT_CHECK_COUPON_API = "products/check_coupon"
    static let PRODUCT_PAYMENT_STATUS_API = "products/payment_status"
    static let REQUIREMENT_MARKETSOLD_API = "requirment/marketsold"
    static let PRODUCTS_VIEWINFO_API = "products/view_info"
    
    
//    static let Getchathistory = SocketUrl! + "/chat/PendingMessages"
    static let CHAT_API = "chat"
    static let CHAT_SEND_MESSAGE_API = "chat/send_message"
    static let CHAT_BLOCK_API = "chat/user_block"
    static let CHAT_UNBLOCK_API = "chat/user_unblock"
    static let CHAT_DELETE_API = "chat/chat_delete"
    
    
    static let MyTransaction_API = "requirment/transaction"
    static let peoplelookingfor_list_API = "peoplelookingfor/peoplelookingfor_list"
       static let MyWishList = "requirment/wishlist"
       static let MyRequirementList = "requirment/RequirmentList"
       static let AddRequirement = "requirment/requirment"
       static let removerequirement = "requirment/remove"
     static let sendchat = "chat/send_message"
     static let sendreport = "requirment/reportadd"
    static let  markandsold = "requirment/marketsold"


 }

extension UserDefaults{
    
    func setIsLogin(value: Bool) {
        set(value, forKey: UserDefaultsKeys.isUserLogin.rawValue)
    }
    
    func getIsLogin()-> Bool {
        return bool(forKey: UserDefaultsKeys.isUserLogin.rawValue)
    }
    
    func setUserDict(value: [String : Any]){
        set(value, forKey: UserDefaultsKeys.userData.rawValue)
    }
    
    func getUserDict() -> [String : Any]{
        return dictionary(forKey: UserDefaultsKeys.userData.rawValue) ?? [:]
    }
    
}

enum UserDefaultsKeys : String {
    case userData
    case isUserLogin
}

