//
//  AppDelegate.swift
//  KIdsSafeStudent
//
//  Created by Mac on 27/06/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import GoogleSignIn

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {



    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        IQKeyboardManager.shared.enable = true
        return true
        
    }
     func application(_ application: UIApplication,
                            open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
               
    //           return  GIDSignIn.sharedInstance().handle(url,sourceApplication: sourceApplication,
    //                    annotation: annotation)
            
             return (GIDSignIn.sharedInstance()?.handle(url))!
           }

           func application(_ application: UIApplication, open url: URL,
                            options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool
           {
               
    //           let googleDidHandle = GIDSignIn.sharedInstance().handle(url,
    //                                                                   sourceApplication:options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
    //                                                                   annotation: options[UIApplication.OpenURLOptionsKey.annotation])
               
            
            let googleDidHandle = (GIDSignIn.sharedInstance()?.handle(url))!
               
               return googleDidHandle
           }
        
        
        }
    
    
    

    // MARK: UISceneSession Lifecycle

//    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
//        // Called when a new scene session is being created.
//        // Use this method to select a configuration to create the new scene with.
//        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
//    }
//
//    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
//        // Called when the user discards a scene session.
//        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
//        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
//    }




