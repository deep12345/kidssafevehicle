//
//  ViewController.swift
//  KIdsSafeStudent
//
//  Created by Mac on 27/06/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import M13Checkbox
import GoogleSignIn
import  IBAnimatable
import IQKeyboardManagerSwift

class ViewController: UIViewController,GIDSignInDelegate {

    @IBOutlet weak var chkbxadmin: M13Checkbox!
    
   
    @IBOutlet weak var txtemail: AnimatableTextField!
    
    
    @IBOutlet weak var txtPassword: AnimatableTextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        GIDSignIn.sharedInstance()?.presentingViewController = self
        
        chkbxadmin?.markType = .checkmark
        chkbxadmin?.boxType = .square
              // chkbxadmin?.tintColor = GlobalColor.SelectedColor!
               // chkbxadmin?.secondaryTintColor = GlobalColor.SelectedColor!
        chkbxadmin?.addTarget(self, action: #selector(ViewController.checkboxValueChanged(_:)), for: .valueChanged)
     
        
        // Do any additional setup after loading the view.
    }
  
    @IBAction func btn_Login(_ sender: Any) {
       validation()
    }
    
    
    
    @IBAction func checkboxValueChanged(_ sender: M13Checkbox) {
        
       // print("TAG:",sender.tag)
        switch sender.checkState {
            
                 case .unchecked:
                    print("UnChecked")
                    //IsTC = "0"
                     break
                 case .checked:
                     print("Checked")
                    // IsTC = "1"
                     break
                 case .mixed:
                     print("Mixed")
                     
                     break
                 }
        
    }
    
    
    
    
    @IBAction func btn_googleClick(_ sender: Any) {
        GIDSignIn.sharedInstance().clientID = "402525413224-vq1bd3h3gbm8loeebre5ceg6pibitgp8.apps.googleusercontent.com"
               
               GIDSignIn.sharedInstance().delegate = self
             //  GIDSignIn.sharedInstance().uiDelegate = self
               GIDSignIn.sharedInstance().signIn()
    }
    
    
    @IBAction func btn_facebookclick(_ sender: Any) {
        
    }
    
    @IBAction func btn_appleClick(_ sender: Any) {
        
    }
    
     func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
            
        }
        
    func sign(_ signIn: GIDSignIn!,present viewController: UIViewController!)
    {
            self.present(viewController, animated: true, completion: nil)
        }
        
        func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
            if let error = error {
                print(error)
                
                return
            }
            guard user.authentication != nil else { return }
            
            let u_id = user.userID
            let u_name = user.profile.name
            let u_fname = user.profile.givenName
            let u_Lname = user.profile.familyName
            //let u_email = user.profile.email
            
            var imageURL = ""
            
            if user.profile.hasImage {
                imageURL = user.profile.imageURL(withDimension: 100).absoluteString
                
                   // self.imageView.image = UIImage(data: NSData(contentsOfURL: url)!)
                
             //   profileImage.sd_setImage(with: URL(string: imageURL), placeholderImage: UIImage(named: "no-image-box.png"))
            }
            
          //  let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken,accessToken: authentication.accessToken)
         var Dict : [String : AnyObject] = [:]
         Dict = ["picture" : imageURL as AnyObject,
                 "name" : u_name as AnyObject,
                 "first_name" : u_fname as AnyObject,
                 "last_name" : u_Lname as AnyObject,
                 "email" : user?.profile.email as AnyObject ,
                 "id" : u_id as AnyObject
         ]
         
         print(Dict)
         
         fetchUserProfileGoogle(dict: Dict as NSDictionary)
    
        }
    
    
    
    func fetchUserProfileGoogle(dict:NSDictionary)
        
       {
        
                let att  = dict as! [String:Any]
                let email = att["email"] as! String
                let name = att["name"] as! String
                let id = att["id"] as! String
                let image = att["picture"] as! String
                let parameters = ["image":image,"email" : email, "name" : name, "type" : "google", "id" : id,"DeviceID" : UIDevice.current.identifierForVendor!.uuidString,"PlayerID" : "123456"
                ] as [String : Any]
           //     let url = ServiceList.SERVICE_URL!+ServiceList.SOCIAL_LOGIN_API
//                self.callApi(url,method: .post,
//                                  param: parameters, withLoader: true)
//                          { (result) in
//                               print("LOGINRESPONSE:",result)
//                               if result.getBool(key: "status")
//                               {
//
//                                let dict = result.getDictionary(key: "data")
//                                 var dictuser = dict.getDictionary(key: "user")
//                                for (key, value) in dictuser {
//                                    let val : NSObject = value as! NSObject;
//                                    dictuser[key] = val.isEqual(NSNull()) ? "" : value
//                                }
//                              //  dictuser["login_token"] = dict.getString(key: "login_token")
//                                print(dictuser)
//                                UserDefaults.standard.setUserDict(value: dictuser)
//                                UserDefaults.standard.setIsLogin(value: true)
//                                UserDefaults.standard.set(dict.getString(key: "login_token"), forKey: "login_token")
//
//                                let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
//                                    self.navigationController?.pushViewController(nextViewController, animated: true)
//                               }
//                            else
//                               {
//                                showToast(uiview: self, msg: result.getString(key: "message"))
//                            }
//                   }
//
//
//
//       }
        
        
}
    
    func validation()
          {
              self.view.endEditing(true)
              if txtemail.text == "" && txtPassword.text == ""
              {
               showToast(uiview: self, msg: "Please enter all fields")
                
              }
                  
              else if !txtemail.isValidEmail()
              {
               showToast(uiview: self, msg: "Please enter  valid EmailId")
              }
                  
              else if txtPassword.text == ""
              {
               showToast(uiview: self, msg: "Please enter password")
              }
                  
              else
              {
                  LoginApi()
              }
          }
    
    func LoginApi()
              {
                  if !isInternetAvailable(){
                      noInternetConnectionAlert(uiview: self)
                  }
                  else
                  {
                   let parameters = ["email" : txtemail.text!, "password" : txtPassword.text!,"device_token" : UIDevice.current.identifierForVendor!.uuidString,"PlayerID" : "123456"
                          ] as [String : Any]
                    let url = ServiceList.SERVICE_URL+ServiceList.LOGIN_API
                       callApi(url,method: .post,param: parameters, withLoader: true)
                             { (result) in
                                  print("LOGINRESPONSE:",result)
                                  if result.getBool(key: "status")
                                  {
                                
                                   let dict = result.getDictionary(key: "data")
                                   var dictuser = dict.getDictionary(key: "user")
                                   for (key, value) in dictuser {
                                       let val : NSObject = value as! NSObject;
                                       dictuser[key] = val.isEqual(NSNull()) ? "" : value
                                   }
                                 //  dictuser["login_token"] = dict.getString(key: "login_token")
                                   print(dictuser)
                                   UserDefaults.standard.setUserDict(value: dictuser)
                                   UserDefaults.standard.setIsLogin(value: true)
                                   UserDefaults.standard.set(dict.getString(key: "login_token"), forKey: "login_token")
                                   
                                   let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
                                       self.navigationController?.pushViewController(nextViewController, animated: true)
                                  }
                               else
                                  {
                                   showToast(uiview: self, msg: result.getString(key: "message"))
                               }
                      }
                  }
                  
              }
}
