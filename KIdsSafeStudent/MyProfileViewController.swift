//
//  MyProfileViewController.swift
//  KIdsSafeStudent
//
//  Created by Mac on 13/07/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit
import IBAnimatable
import SDWebImage
import OpalImagePicker


class MyProfileViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,OpalImagePickerControllerDelegate {

    
   
    
    @IBOutlet weak var txtName: AnimatableTextField!
    
    @IBOutlet weak var txtlastname: AnimatableTextField!
    
    @IBOutlet weak var txtEmail: AnimatableTextField!
    
    
    @IBOutlet weak var txtPhoneNo: AnimatableTextField!
    
    
    @IBOutlet weak var txtLIcenseImage: UIImageView!
    
    @IBOutlet weak var btnLicense: AnimatableButton!
    
      let imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()

       getProfile()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btn_updateProfile(_ sender: Any) {
        
        validation()
    }
    
    
    @IBAction func btnUpdateLicense(_ sender: Any) {
        let alertViewController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                    
        
        if let popoverController = alertViewController.popoverPresentationController {
            popoverController.sourceView = self.view //to set the source of your alert
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0) // you can set this as per your requirement.
                popoverController.permittedArrowDirections = [] //to hide the arrow of any particular direction
                                              }
                let camera = UIAlertAction(title: "Take Photo", style: .default, handler: { (alert) in
                                 self.openCamera()
                             })
                             let gallery = UIAlertAction(title: "Choose from Gallery", style: .default) { (alert) in
                                 self.openGallary()
                             }
                             let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (alert) in
                                 
                             }
                             alertViewController.addAction(camera)
                             alertViewController.addAction(gallery)
                             alertViewController.addAction(cancel)
                             self.present(alertViewController, animated: true, completion: nil)
                         }
        
    
    
    
    @IBAction func btnBack(_ sender: Any) {
      
        navigationController?.popViewController(animated: true)
        
    }

    
    func SetData()
    {
     
        txtName.text  = UserDefaults.standard.getUserDict()["first_name"] as? String ?? ""
         txtlastname.text  = UserDefaults.standard.getUserDict()["last_name"] as? String ?? ""
        txtEmail.text = UserDefaults.standard.getUserDict()["email"] as? String ?? ""
        txtPhoneNo.text = UserDefaults.standard.getUserDict()["mobile_no"] as? String ?? ""
        
         let imgURL = UserDefaults.standard.getUserDict()["LicenceURL"] as? String ?? ""
        
        //  self.btnphoto.sd_setBackgroundImage(with: URL(string:imgURL), for: .normal)
          
      self.btnLicense.sd_setImage(with: URL(string:imgURL), for: .normal, placeholderImage: #imageLiteral(resourceName: "Group 6231-1"), options: .progressiveLoad, completed: nil)
//
//        txtLIcenseImage.sd_setImage(with: URL(string:imgURL), completed: nil)
        
        
    }
    
    func getProfile()
       {
           if !isInternetAvailable(){
           noInternetConnectionAlert(uiview: self)
                       }
               else
               {
                    let parameters = ["email" : UserDefaults.standard.getUserDict()["email"] as? String ?? "", "password" :UserDefaults.standard.getUserDict()["password"] as? String ?? "","device_token" : UIDevice.current.identifierForVendor!.uuidString]
               
                let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                "X-KIDSAFE-LOGIN-TOKEN":UserDefaults.standard.object(forKey: "login_token") ?? ""
                                         ] as [String : Any]
                                         
                   let url = ServiceList.SERVICE_URL+ServiceList.getProfile
                         
                    callApi(url,method: .get,
                            param: parameters, extraHeader: header,
                            withLoader: true)
                                                  { (result) in
                                                     //  print("LOGINRESPONSE:",result)
                                                       if result.getBool(key: "status")
                                                       {
                                               
                                                        
                            let dict = result.getDictionary(key: "data")
                            var dictuser = dict.getDictionary(key: "user")
                                
                        for (key, value) in dictuser {
                        
                            let val : NSObject = value as! NSObject;
                            dictuser[key] = val.isEqual(NSNull()) ? "" : value
                            
                                                        
                                                        
                                                        }
                                                                                       //  dictuser["login_token"] = dict.getString(key: "login_token")
                                                                                         print(dictuser)
                                                        
          
                                                        
//        self.arrStudentslist = result["data"] as? [[String:Any]] ?? []
//        print(self.arrStudentslist)
//        self.tblstudentList.reloadData()
                      
            self.txtName.text  = dictuser["first_name"] as? String ?? ""
            self.txtlastname.text  = dictuser["last_name"] as? String ?? ""
            self.txtEmail.text = dictuser["email"] as? String ?? ""
            self.txtPhoneNo.text = dictuser["mobile_no"] as? String ?? ""
                                                               
            let imgURL = dictuser["LicenceURL"] as? String ?? ""
                                                               
                                                               //  self.btnphoto.sd_setBackgroundImage(with: URL(string:imgURL), for: .normal)
                                                                 
                    self.btnLicense.sd_setImage(with: URL(string:imgURL), for: .normal, placeholderImage: #imageLiteral(resourceName: "Group 6231-1"), options: .progressiveLoad, completed: nil)
                                                       
                                                       }
                                        else
                                                    {
                                                        showToast(uiview: self, msg: result.getString(key: "message"))
                                                    }
                                           }
                                       }
                                       
                                   }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    func UpdateProfileApi()
                   {
                       if !isInternetAvailable(){
                           noInternetConnectionAlert(uiview: self)
                       }
                       else
                       {
                     let parameters = [  "first_name" : txtName.text ?? "",
                                         "last_name" : txtlastname.text ?? "",
                                        "email" : txtEmail.text  ?? "",
                                              // "password" : txtpassword.text ?? "",
                        "mobile_no" : txtPhoneNo.text ?? "",
                                //      "DeviceID" : UIDevice.current.identifierForVendor!.uuidString,
                                             
                                      ] as [String : Any]
                          
                          let header = ["USER-ID": UserDefaults.standard.getUserDict()["id"] as? String ?? "",
                           "X-KIDSAFE-LOGIN-TOKEN":UserDefaults.standard.object(forKey: "login_token") ?? "" ,
                                                    ] as [String : Any]
                       
                        
                        
                          let url = ServiceList.SERVICE_URL + ServiceList.UpdateProfile_API
                        let imgdata = btnLicense.imageView?.image?.jpegData(compressionQuality: 0.5)
                        
                            callApi(url,
                                    method: .post,
                                    param: parameters ,
                                    extraHeader: header,
                                    withLoader: true,
                                    data: [imgdata!],
                                    dataKey: ["license_photo"])
                                
                                  { (result) in
                                  //     print("LOGINRESPONSE:",result)
                                       if result.getBool(key: "status")
                                       {
                         
                    showToast(uiview: self, msg: result.getString(key: "message"))
                    
                    let dict = result.getDictionary(key: "data")
                    var dictuser = dict.getDictionary(key: "user")
                                                
                        for (key, value) in dictuser {
                                        
                        let val : NSObject = value as! NSObject;
                        dictuser[key] = val.isEqual(NSNull()) ? "" : value
                                            
                                                                        
                                                                        
                                                                        }
                                                                                                       //  dictuser["login_token"] = dict.getString(key: "login_token")
                                                                                                         print(dictuser)
                                                                        
                          
                                                                        
                //        self.arrStudentslist = result["data"] as? [[String:Any]] ?? []
                //        print(self.arrStudentslist)
                //        self.tblstudentList.reloadData()
                                      
                            self.txtName.text  = dictuser["first_name"] as? String ?? ""
                            self.txtlastname.text  = dictuser["last_name"] as? String ?? ""
                            self.txtEmail.text = dictuser["email"] as? String ?? ""
                            self.txtPhoneNo.text = dictuser["mobile_no"] as? String ?? ""
                                                                               
                            let imgURL = dictuser["LicenceURL"] as? String ?? ""
                                                                               
                                                                               //  self.btnphoto.sd_setBackgroundImage(with: URL(string:imgURL), for: .normal)
                                                                                 
                                    self.btnLicense.sd_setImage(with: URL(string:imgURL), for: .normal, placeholderImage: #imageLiteral(resourceName: "Group 6231-1"), options: .progressiveLoad, completed: nil)
                                       
                                       }
                                    else
                                       {
                    showToast(uiview: self, msg: result.getString(key: "message"))
                                        
                                    }
                           }
                       }
                       
                   }
    
    
    func openCamera()
         {
                if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
                              let imagePicker = UIImagePickerController()
                              imagePicker.delegate = self as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
                              imagePicker.allowsEditing = true
                              imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
                              self.present(imagePicker, animated: true, completion: nil)
                          }
                          else
                          {
                              let alert  = UIAlertController(title: "Warning", message: "You don't have permission to access gallery.", preferredStyle: .alert)
                              alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                              self.present(alert, animated: true, completion: nil)
                          }
                  
               }
               
               func openGallary() {
                   
                if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary){
                              let imagePicker = UIImagePickerController()
                              imagePicker.delegate = self as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
                              imagePicker.allowsEditing = true
                              imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
                              self.present(imagePicker, animated: true, completion: nil)
                          }
                          else
                          {
                              let alert  = UIAlertController(title: "Warning", message: "You don't have permission to access gallery.", preferredStyle: .alert)
                              alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                              self.present(alert, animated: true, completion: nil)
                          }
               }
               
               //imagePickerController
        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])         {
                   // assign pickup image into varibale
               if let image = info[.originalImage] as? UIImage {
                       
                       self.btnLicense.contentMode = .scaleAspectFill
                       self.btnLicense.layer.masksToBounds = true
                       self.btnLicense.setImage(image, for: .normal)
                       
                   }
                   
                   self.dismiss(animated: true, completion: { () -> Void in
                     

                   })
               }

               //imagePickerControllerDidCancel
               public func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
               {
                   dismiss(animated: true, completion: nil)
               }
    
    
    func validation()
            {
                self.view.endEditing(true)
                if txtEmail.text == "" && txtPhoneNo.text == "" && txtName.text == ""
                {
                 showToast(uiview: self, msg: "Please enter all fields")
                  
                }
                    
                else if !txtEmail.isValidEmail()
                {
                 showToast(uiview: self, msg: "Please enter valid EmailId")
                }
                    
                else if txtPhoneNo.text == ""
                {
                 showToast(uiview: self, msg: "Please enter Phone No")
                }
                    else if txtName.text == ""
                    {
                        showToast(uiview: self, msg: "Please enter Name")
                    }
                    
                else
                {
                    UpdateProfileApi()
                }
            }
    
    
    
    
    
    
}


